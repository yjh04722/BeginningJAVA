1. 다음 조건식을 조건문(if문)으로 바꾸어보세요

     grade = (sore >= 90) ? 'A' : 'B';

```
char grade;
if( score >= 90){
	grade = 'A';
}
else{
	grade = 'B';
}
```


2. 구구단을 짝수 단만 출력하도록 프로그램을 만들어 모세요 (continue문 사용)

```
public class Q2 {

	public static void main(String[] args) {

		int dan;
		int times;
		
		for(dan = 2; dan <=9; dan++){
			if (dan %2 != 0) continue;
			
			for(times = 1; times <=9; times++){
				System.out.println(dan + "X" + times + "=" + dan * times);
			}
			System.out.println();
		}
	}
	
}
```


3. 구구단을 단보다 곱하는 수가 작거나 같은 경우까지만 출력하는 프로그램을 만들어 보세요

```
public class Q3 {
	public static void main(String[] args) {

		int dan;
		int times;
		
		for(dan = 2; dan <=9; dan++){
			for(times = 1; times <= dan; times++){
				System.out.println(dan + "X" + times + "=" + dan * times);
				
			}
			System.out.println();
			
		}

	}
}
```


4. 조건문과 반복문을 활용하여 다이아몬드를 출력해 보세요

![diamond](./img/diamond.png)    


```
public class Q4 {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		
		int lineCount = scanner.nextInt(); 
		int spaceCount = lineCount/2 +1;
		int starCount = 1;
		
		for(int i = 0; i<lineCount; i++) {
			for(int j = 0; j<spaceCount; j++) {
				System.out.print(' ');
			}
			for(int j=0; j<starCount; j++) {
				System.out.print('*');
			}
			for(int j = 0; j<spaceCount; j++) {
				System.out.print(' ');
			}
			
			if(i < lineCount/2) {
				spaceCount-=1;
				starCount+=2;
			}
			else {
				spaceCount+=1;
				starCount-=2;
			}
			System.out.println();
		}
	}
}
``` 



